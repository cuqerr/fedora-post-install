#!/bin/bash

#Copying all files to home directory
#mkdir ~/FedoraPostInstall_By_cuqer/
#cp -r * ~/FedoraPostInstal_By_cuqer/

UPDATECHECKER=updated.check
if [ -f "$UPDATECHECKER" ]; then

#INSTALLING RPM FUSION AND FEDY
# RPM Fusion
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
# Install fedy copr repository
sudo dnf copr enable kwizart/fedy
# Install fedy
sudo dnf install fedy -y

#Make the cache ;)
sudo dnf makecache
#Install the packages(configurable in install.packages)
sudo dnf install $(cat install.packages) -y
#Installing multimedia related stuff
sudo dnf install gstreamer1-plugins-base gstreamer1-plugins-good gstreamer1-plugins-ugly gstreamer1-plugins-bad-free gstreamer1-plugins-bad-free gstreamer1-plugins-bad-freeworld gstreamer1-plugins-bad-free-extras ffmpeg ffmpegthumbnailer -y
sudo dnf group install multimedia -y
#Battery optimisation tool for laptops
#sudo dnf install tlp -y
#Extra cool stuff
sudo dnf install libreoffice -y
sudo dnf install wine yakuake icedtea-web java-openjdk p7zip p7zip-plugins unrar redshift -y
#Drivers for exFAT etc.
sudo dnf install exfat fuse-exfat -y

#MANUAL: ADD LINES TO /etc/dnf/dnf.conf
#fastestmirror=true
#deltarpm=true
#fastestmirror: makes you connect to the fastest mirror
#deltarpm: only downloads changed files when updating packages, at the cost of CPU power.

#Different DE's
#sudo dnf install @lxde-desktop
#sudo dnf install @xfce-desktop
#sudo dnf install @cinnamon-desktop
#sudo dnf install @kde-desktop
#sudo dnf install @mate-desktop


else 
    echo "This script will update your system and then reboot it. You can run this script again in order to continue the installation."	
	sleep 2
	touch updated.check
	sudo dnf check-update
	sudo dnf upgrade -y
	sudo reboot
fi
